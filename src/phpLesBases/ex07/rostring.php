<?php
// je vérifie que le nombre d'éléments est supérieur à 1
if ($argc > 1){
    //je récupère l'élément du tableau qui m'intéresse
    array_slice($argv,1);

    // je donne une valeur a la chaine de commande qui m'intéresse
    $chaine=$argv[1];

    // je split ma chaine de commande pour la rendre lisible
    $tab=preg_split("/[^\S\r\n]/",$chaine,-1,PREG_SPLIT_NO_EMPTY);

    // je donne une valeur au premier mot de la chaine
    $debut=$tab[0];

    // j'extraie la portion du tableau qui m'intéresse
    $tab=array_slice($tab,1);

    // je créé un tableau en rajoutant mon mot à la fin de mon tableau
    array_push($tab,$debut);
    
    // je transforme mon tableau en string avec le mot du début déplacé à la fin
    echo implode(" ",$tab)."\n";
}